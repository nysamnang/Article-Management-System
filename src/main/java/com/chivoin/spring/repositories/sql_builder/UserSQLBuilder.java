package com.chivoin.spring.repositories.sql_builder;

import com.chivoin.spring.entities.filter.UserFilter;
import org.apache.ibatis.annotations.Param;

/**
 * Created by Obi-Voin Kenobi on 06-Jul-17.
 */
public class UserSQLBuilder {

    public static String userFilter(@Param("user") UserFilter user){
        StringBuffer buffer = new StringBuffer();
        buffer.append("SELECT "+
                "U.id, U.username, U.email, U.password, " +
                "U.dob, U.gender, U.created_date, U.status, U.uuid " +
                "FROM restful_db.users U ");

        if (user.getUsername() == null && user.getEmail() == null && user.getStatus() == null &&
                (user.getFrom_created_date() == null || user.getTo_created_date() == null))
            return "";
        else {
            boolean isContinue = false;
            buffer.append("WHERE ");

            if(user.getUsername() != null){
                buffer.append("username LIKE '%' || #{user.username} || '%' ");
                isContinue = true;
            }
            if(user.getEmail() != null) {
                if (isContinue)
                    buffer.append("OR email LIKE '%' || #{user.email} || '%' ");
                else {
                    buffer.append("email LIKE '%' || #{user.email} || '%' ");
                    isContinue = true;
                }
            }
            if(user.getStatus() != null) {
                if (isContinue)
                    buffer.append("OR status LIKE '%' || #{user.status} || '%' ");
                else {
                    buffer.append("status LIKE '%' || #{user.status} || '%' ");
                    isContinue = true;
                }
            }
            if(user.getFrom_created_date() != null && user.getTo_created_date() != null) {
                if (isContinue)
                    buffer.append("OR created_date >= #{user.from_created_date} " +
                            "AND created_date <= #{user.to_created_date}");
                else
                    buffer.append("created_date >= #{user.from_created_date} " +
                            "AND created_date <= #{user.to_created_date}");
            }
        }
        return buffer.toString();
    }

}

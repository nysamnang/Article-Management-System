package com.chivoin.spring.repositories;

import com.chivoin.spring.entities.Role;
import com.chivoin.spring.entities.User;
import com.chivoin.spring.entities.filter.UserFilter;
import com.chivoin.spring.entities.form.UserForm;
import com.chivoin.spring.repositories.sql_builder.UserSQLBuilder;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Obi-Voin Kenobi on 05-Jul-17.
 */
@Repository
public interface UserRepository {

    @Select("SELECT " +
            "U.id, U.username, U.email, U.password, U.dob, " +
            "U.gender, U.created_date, U.status, U.device, U.uuid " +
            "FROM restful_db.users U " +
            "WHERE U.status = '1';")
    @Results(value={
            @Result(column = "created_date", property = "createdDate"),
            @Result(column = "id", property = "id"),
            @Result(property="roles", column="id",
                    many = @Many(select  = "findRolesByUserId")
            )
    })
    List<User> findAllUsers();

    @SelectProvider(type = UserSQLBuilder.class, method = "userFilter")
    @Results(value={
            @Result(property="createdDate",column="created_date"),
            @Result(property="roles", column="id",
                    many = @Many(select  = "findRolesByUserId")
            )
    })
    List<User> filterUsers(@Param("user") UserFilter user);

    @Select("SELECT " +
            "U.id, U.username, U.email, U.password, " +
            "U.dob, U.gender, U.created_date, U.status, U.uuid " +
            "FROM restful_db.users U " +
            "WHERE U.status = '1' AND U.uuid = #{uuid};")
    @Results(value={
            @Result(column = "created_date", property = "createdDate"),
            @Result(column = "id", property = "id"),
            @Result(property="roles", column="id",
                    many = @Many(select  = "findRolesByUserId")
            )
    })
    User findUsersByUUID(@Param("uuid") String uuid);

    @Select("SELECT R.id, R.name, R.created_date, R.index " +
            "FROM restful_db.roles R " +
            "INNER JOIN restful_db.user_roles UR " +
            "ON R.id = UR.role_id " +
            "WHERE UR.user_id = #{user_id} AND UR.status = '1'")
    @Results(value={
            @Result(property="createdDate",column="created_date")
    })
    List<Role> findRolesByUserId(@Param("user_id") int userId);

    @Update("UPDATE restful_db.users " +
            "SET username = #{U.username}, email = #{U.email}, password = #{U.password}, " +
            "dob = #{U.dob}, gender = #{U.gender}, device = #{U.device} " +
            "WHERE uuid = #{U.uuid}")
    @SelectKey(
            statement = "SELECT id FROM restful_db.users WHERE uuid = #{U.uuid}",
            keyColumn = "id",
            keyProperty = "U.id",
            before = false,
            resultType = int.class)
    boolean updateUser(@Param("U") UserForm user);

    @Update("UPDATE restful_db.users SET status=#{status} WHERE uuid=#{uuid}")
    boolean updateUserStatus(@Param("uuid") String uuid, @Param("status") String status);

    @Delete("DELETE FROM restful_db.users WHERE uuid=#{uuid}")
    boolean deleteUserByUUID(@Param("uuid") String uuid);

    @Insert("INSERT INTO restful_db.users " +
            "(username, email, password, dob, gender, device, uuid, status) " +
            "VALUES " +
            "(#{user.username}, #{user.email}, #{user.password}, #{user.dob}, " +
            "#{user.gender}, #{user.device}, #{user.uuid}, '1')")
    @SelectKey(
            statement = "SELECT last_value FROM restful_db.users_id_seq",
            keyColumn = "last_value",
            keyProperty = "user.id",
            before = false,
            resultType = int.class)
    boolean addUser(@Param("user") UserForm user);
}

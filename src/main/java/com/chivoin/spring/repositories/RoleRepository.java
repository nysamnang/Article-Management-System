package com.chivoin.spring.repositories;

import com.chivoin.spring.entities.Role;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Obi-Voin Kenobi on 07-Jul-17.
 */
@Repository
public interface RoleRepository {

    @Update("UPDATE restful_db.user_roles SET status='0' WHERE user_id=#{user_id}")
    boolean disableUserRolesByUserId(@Param("user_id") int user_id);

    @Update("<script>" +
                "INSERT INTO restful_db.user_roles (user_id, role_id) VALUES " +
                "<foreach collection='role_list' item='role' separator=','>" +
                    " (#{user_id}, #{role.id})" +
                "</foreach>" +
            "</script>")
    boolean addUserRolesByUserId(@Param("user_id") int user_id, @Param("role_list") List<Role> role_id);
}

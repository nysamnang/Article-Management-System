package com.chivoin.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArticlemanagementsystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArticlemanagementsystemApplication.class, args);
	}
}

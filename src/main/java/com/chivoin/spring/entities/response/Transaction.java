package com.chivoin.spring.entities.response;

/**
 * Created by Obi-Voin Kenobi on 05-Jul-17.
 */
public class Transaction {

    public static final String SUCCESS_CREATED = "created";
    public static final String SUCCESS_RETRIEVED = "retrieved";
    public static final String SUCCESS_UPDATED = "updated";
    public static final String SUCCESS_DELETED = "deleted";

    public static final String FAILURE_CREATE = "create";
    public static final String FAILURE_RETRIEVE = "retrieve";
    public static final String FAILURE_UPDATE = "update";
    public static final String FAILURE_DELETE = "delete";

}

package com.chivoin.spring.entities.response;

import java.util.List;

/**
 * Created by Obi-Voin Kenobi on 05-Jul-17.
 */
public class ResponseRecords<T> extends Response {

    private List<T> data;

    public ResponseRecords() {
        super();
    }

    public ResponseRecords(String message, boolean status, List<T> data) {
        super(message, status);
        this.data = data;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}

package com.chivoin.spring.entities.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Obi-Voin Kenobi on 05-Jul-17.
 */
public class Response<T> {

    @JsonProperty("message")
    private String message;

    @JsonProperty("status")
    private boolean status;

    public Response() { }

    public Response(String message, boolean status) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}

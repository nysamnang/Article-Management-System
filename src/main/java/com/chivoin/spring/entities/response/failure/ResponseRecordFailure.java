package com.chivoin.spring.entities.response.failure;

import com.chivoin.spring.entities.response.ResponseHTTPStatus;
import com.chivoin.spring.entities.response.ResponseRecord;
import com.chivoin.spring.entities.response.ResponseRecords;

import java.util.Collections;

/**
 * Created by Obi-Voin Kenobi on 05-Jul-17.
 */
public class ResponseRecordFailure<T> extends ResponseRecord {

    private ResponseFailure.Error error;

    public ResponseRecordFailure() { }

    public ResponseRecordFailure(String message, boolean status, ResponseHTTPStatus error) {
        super.setMessage(message);
        super.setStatus(status);
        super.setData(null);
        this.setError(new ResponseFailure.Error(error));
    }

    public ResponseFailure.Error getError() {
        return error;
    }

    public void setError(ResponseFailure.Error error) {
        this.error = error;
    }
}

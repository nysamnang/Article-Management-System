package com.chivoin.spring.entities.response.failure;

import com.chivoin.spring.entities.response.Response;
import com.chivoin.spring.entities.response.ResponseHTTPStatus;

/**
 * Created by Obi-Voin Kenobi on 05-Jul-17.
 */
public class ResponseFailure<T> extends Response {

    private Error error;

    public ResponseFailure(String message, boolean status, ResponseHTTPStatus error) {
        super.setMessage(message);
        super.setStatus(status);
        this.setError(new Error(error));
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public static class Error {

        private int code;
        private String message;

        public Error(ResponseHTTPStatus status) {
            super();
            this.code = status.value();
            this.message = status.getReasonPhrase();
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}

package com.chivoin.spring.entities.response.failure;

import com.chivoin.spring.entities.response.ResponseHTTPStatus;
import com.chivoin.spring.entities.response.ResponseRecords;

import java.util.Collections;

/**
 * Created by Obi-Voin Kenobi on 05-Jul-17.
 */
public class ResponseRecordsFailure<T> extends ResponseRecords {

    ResponseFailure.Error error;

    public ResponseRecordsFailure() {
    }

    public ResponseRecordsFailure(String message, boolean status, ResponseHTTPStatus error) {
        super.setMessage(message);
        super.setStatus(status);
        super.setData(Collections.emptyList());
        this.setError(new ResponseFailure.Error(error));
    }

    public ResponseFailure.Error getError() {
        return error;
    }

    public void setError(ResponseFailure.Error error) {
        this.error = error;
    }
}

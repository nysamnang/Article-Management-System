package com.chivoin.spring.entities.response;

/**
 * Created by Obi-Voin Kenobi on 05-Jul-17.
 */
public class HTTPMessage {

    /**
     * Static method for generating a Success Message
     * @param Table table name
     * @param Transaction transaction action
     * @return Success Message for Http Response
     */
    public static String getSuccessMessage(String Table, String Transaction) {
        return Table + " has been " + Transaction + " successfully.";
    }

    /**
     * Static method for generating a Failure Message
     */
    public static String getFailureMessage(String Table, String Transaction) {
        return Table + " could not " + Transaction + ".";
    }

    /**
     * Static method for generating a Invalid data input Message
     */
    public static String getInvalidMessage(String Table, String Transaction, String invalidMessage) {
        return Table + " could not " + Transaction + ". " + invalidMessage;
    }
}

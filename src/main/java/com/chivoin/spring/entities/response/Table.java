package com.chivoin.spring.entities.response;

/**
 * Created by Obi-Voin Kenobi on 05-Jul-17.
 */
public class Table {

    public static final String ARTICLE_TAG = "Article_Tags";
    public static final String ARTICLES = "Article(s)";
    public static final String CATEGORIES = "Category(s)";
    public static final String COMMENTS = "Comment(s)";
    public static final String FILES = "File(s)";
    public static final String PERMISSIONS = "Permission(s)";
    public static final String ROLE_PERMISSIONS = "Role_Permissions";
    public static final String ROLES = "Role(s)";
    public static final String TAGS = "Tag(s)";
    public static final String USER_ROLES = "User_Roles";
    public static final String USERS = "User(s)";

}

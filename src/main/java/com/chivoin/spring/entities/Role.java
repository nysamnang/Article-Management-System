package com.chivoin.spring.entities;

import java.sql.Timestamp;

/**
 * Created by Obi-Voin Kenobi on 05-Jul-17.
 */
public class Role extends BaseEntity{

    private int countUser;

    public Role() {
        super();
    }

    public Role(int id, String name, String remark, String status, Timestamp createdDate, int index, String uuid, int countUser) {
        super(id, name, remark, status, createdDate, index, uuid );
        this.countUser = countUser;
    }

    public int getCountUser() {
        return countUser;
    }

    public void setCountUser(int countUser) {
        this.countUser = countUser;
    }
}

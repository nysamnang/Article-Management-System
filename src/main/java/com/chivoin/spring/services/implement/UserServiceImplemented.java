package com.chivoin.spring.services.implement;

import com.chivoin.spring.entities.User;
import com.chivoin.spring.entities.filter.UserFilter;
import com.chivoin.spring.entities.form.UserForm;
import com.chivoin.spring.repositories.RoleRepository;
import com.chivoin.spring.repositories.UserRepository;
import com.chivoin.spring.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by Obi-Voin Kenobi on 05-Jul-17.
 */
@Service
public class UserServiceImplemented implements UserService{

    private UserRepository userRepository;
    private RoleRepository roleRepository;

    @Autowired
    public UserServiceImplemented(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository =  roleRepository;
    }

    @Override
    public List<User> findAllUsers() {
        return userRepository.findAllUsers();
    }

    @Override
    public List<User> filterUsers(UserFilter user) {
        return userRepository.filterUsers(user);
    }

    @Override
    public User findUsersByUUID(String uuid) {
        return userRepository.findUsersByUUID(uuid);
    }

    @Override
    public boolean updateUserStatus(String uuid, String status) {
        return userRepository.updateUserStatus(uuid, status);
    }

    @Override
    public User updateUser(UserForm user) {
        if (userRepository.updateUser(user)) {
            if (roleRepository.disableUserRolesByUserId(user.getId())) {
                if (roleRepository.addUserRolesByUserId(user.getId(), user.getRoles())) {
                    return userRepository.findUsersByUUID(user.getUuid());
                }
            }
        }
        return null;
    }

    @Override
    public boolean deleteUserByUUID(String uuid) {
        return userRepository.deleteUserByUUID(uuid);
    }

    @Override
    public User addUser(UserForm user) {
        user.setUuid(UUID.randomUUID().toString());
        if (userRepository.addUser(user)) {
            if (roleRepository.addUserRolesByUserId(user.getId(), user.getRoles())) {
                return userRepository.findUsersByUUID(user.getUuid());
            }
        }
        return null;
    }

}

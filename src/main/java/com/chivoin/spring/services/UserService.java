package com.chivoin.spring.services;

import com.chivoin.spring.entities.User;
import com.chivoin.spring.entities.filter.UserFilter;
import com.chivoin.spring.entities.form.UserForm;

import java.util.List;

/**
 * Created by Obi-Voin Kenobi on 05-Jul-17.
 */
public interface UserService {

    List<User> findAllUsers();

    List<User> filterUsers(UserFilter user);

    User findUsersByUUID(String uuid);

    boolean updateUserStatus(String uuid, String status);

    User updateUser(UserForm user);

    boolean deleteUserByUUID(String uuid);

    User addUser(UserForm user);

}
